[ -z $apiurl ] && { echo "set \$apiurl" >&2; exit 123; }
[ -z $apikey ] && { echo "set \$apikey" >&2; exit 123; }
[ -z $Id ] && { echo "set \$Id" >&2 && exit 123; }

curl -X GET "$apiurl/devices/$Id" \
     -H "accept: application/json" \
     -H "Authorization: Bearer $apikey"
res=$?
[ "$res" == "0" ] || { echo "Error $0 exits with $res"; exit $res; }
