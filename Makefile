all: push

build:
	docker build --no-cache -t ehirdoy/fota .

push: build
	docker push ehirdoy/fota
