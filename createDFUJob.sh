[ -z $apiurl ] && { echo "set \$apiurl" >&2; exit 123; }
[ -z $apikey ] && { echo "set \$apikey" >&2; exit 123; }
[ -z $Id ] && { echo "set \$Id" >&2; exit 123; }
[ -z $fname ] && { echo "set \$fname" >&2; exit 123; }
ver="v$(date +%Y.%m%d.%H%M)"

curl -X POST "$apiurl/dfu-jobs" \
     -H "accept: */*" \
     -H "Authorization: Bearer $apikey" \
     -H "Content-Type: application/json" \
     -d @- <<EOF
{
	"deviceIdentifiers": ["$Id", "$Id"],
	"filename": "$fname",
	"version":"$ver"
}
EOF
res=$?
[ "$res" == "0" ] || { echo "Error $0 exits with $res"; exit $res; }
