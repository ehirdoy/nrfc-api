[ -z $apiurl ] && { echo "set \$apiurl" >&2; exit 123; }
[ -z $apikey ] && { echo "set \$apikey" >&2; exit 123; }
[ -z $fname ] &&  { echo "set \$fname" >&2; exit 123; }

curl -X DELETE "$apiurl/firmwares/$fname" \
     -H "accept: */*" \
     -H "Authorization: Bearer $apikey"
res=$?
[ "$res" == "0" ] || { echo "Error $0 exits with $res"; exit $res; }
