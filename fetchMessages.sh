[ -z $apiurl ] && { echo "set \$apiurl" >&2; exit 123; }
[ -z $apikey ] && { echo "set \$apikey" >&2; exit 123; }
[ -z $Id ] && { echo "set \$Id" >&2; exit 123; }

url="$apiurl/messages"
url="$url?inclusiveStart=$(date --date yesterday -u -Iseconds | sed 's/+.*//').000Z"
url="$url&exclusiveEnd=$(date -u -Iseconds | sed 's/+.*//').000Z"
url="$url&Identifiers=$Id"

curl -X GET "$url" \
     -H "accept: application/json" \
     -H "Authorization: Bearer $apikey"
res=$?
[ "$res" == "0" ] || { echo "Error $0 exits with $res"; exit $res; }
