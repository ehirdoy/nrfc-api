FROM ubuntu:18.04
RUN apt-get -y update && apt-get -y upgrade && apt-get -y install \
        git \
        curl \
        jq
RUN apt-get -y clean && apt-get -y autoremove
RUN git clone https://gitlab.com/ehirdoy/nrfc-api.git nrfc
RUN mkdir /work
WORKDIR /nrfc
CMD /nrfc/run.sh
