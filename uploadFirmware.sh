[ -z $apiurl ] && { echo "set \$apiurl" >&2; exit 123; }
[ -z $apikey ] && { echo "set \$apikey" >&2; exit 123; }
[ -z $fname ] && { echo "set \$fname" >&2; exit 123; }

curl -X POST "$apiurl/firmwares" \
     -H "accept: */*" \
     -H "Authorization: Bearer $apikey" \
     -H "Content-Type: application/json" \
     -d @- <<EOF
{
	"filename": "$fname",
        "file": "$(base64 $fname | tr -d '\n')"
}
EOF
res=$?
[ "$res" == "0" ] || { echo "Error $0 exits with $res"; exit $res; }
