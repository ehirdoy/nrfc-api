#!/bin/bash -x
cp /work/app_update.bin . || exit 123
source /work/env || exit 123
./deleteFirmwares.sh || exit 123
fname=app_update.bin ./uploadFirmware.sh || exit 123
fname=$(./fetchFirmware.sh) ./createDFUJob.sh || exit 123
while true; do
    sleep 5
    res=$(./fetchDFUJobExecutions.sh  | jq -r '.items[0].summary.status')
    [ $res == "SUCCEEDED" ] && exit 0
done
echo "OK"
